""" Tests for ltbu's run_backup function. """

import json
import os
import pathlib
import shutil
import tempfile
import unittest

import ltbu


class RunBackupTest(unittest.TestCase):
    def setUp(self):
        self.missing_dir = pathlib.Path('/nonexistent')
        self.dest_dir = pathlib.Path(tempfile.mkdtemp())
        self.target_dirs = ['tests/target']

    def tearDown(self):
        shutil.rmtree(str(self.dest_dir))

    def _copy_jsonfile(self, filename):
        shutil.copy(
            'tests/{}'.format(filename),
            str(self.dest_dir / 'Backup' / 'ltbu.json')
        )

    def _check_destination(self):
        backupdir = self.dest_dir / 'Backup'
        metafilepath = backupdir / 'ltbu.json'

        self.assertTrue(backupdir.is_dir())
        self.assertTrue(metafilepath.is_file())

        with open(str(metafilepath)) as metafile:
            metadata = json.load(metafile)

        self.assertEqual(metadata.keys(), {'name', 'lastbackup'})
        self.assertEqual(metadata['name'], 'TestBackup')

        backupcontents = list(backupdir.iterdir())
        backupcontents.remove(metafilepath)
        self.assertEqual(len(backupcontents), 1)
        self.assertTrue(backupcontents[0].is_dir())

        backupfiles = list(backupcontents[0].iterdir())
        self.assertEqual(len(backupfiles), 1)
        self.assertEqual(backupfiles[0].read_text(), 'TESTDATA')

    def test_regular(self):
        os.mkdir(str(self.dest_dir / 'Backup'))
        self._copy_jsonfile('ltbu_working.json')

        ltbu.run_backup(self.dest_dir, self.target_dirs)
        self._check_destination()

    def test_nodate(self):
        os.mkdir(str(self.dest_dir / 'Backup'))
        self._copy_jsonfile('ltbu_nodate.json')

        ltbu.run_backup(self.dest_dir, self.target_dirs)
        self._check_destination()

    def test_missing_destination(self):
        with self.assertRaises(ltbu.LTBUError):
            ltbu.run_backup(self.missing_dir, self.target_dirs)

    def test_missing_backupdir(self):
        with self.assertRaises(ltbu.LTBUError):
            ltbu.run_backup(self.dest_dir, self.target_dirs)

    def test_missing_metafile(self):
        os.mkdir(str(self.dest_dir / 'Backup'))

        with self.assertRaises(ltbu.LTBUError):
            ltbu.run_backup(self.dest_dir, self.target_dirs)

    def test_broken_metafile(self):
        os.mkdir(str(self.dest_dir / 'Backup'))
        self._copy_jsonfile('ltbu_broken.txt')

        with self.assertRaises(ltbu.LTBUError):
            ltbu.run_backup(self.dest_dir, self.target_dirs)

    def test_missingname_metafile(self):
        os.mkdir(str(self.dest_dir / 'Backup'))
        self._copy_jsonfile('ltbu_missingname.json')

        with self.assertRaises(ltbu.LTBUError):
            ltbu.run_backup(self.dest_dir, self.target_dirs)

    def test_missing_target(self):
        os.mkdir(str(self.dest_dir / 'Backup'))
        self._copy_jsonfile('ltbu_working.json')

        with self.assertRaises(ltbu.LTBUError):
            ltbu.run_backup(self.dest_dir, ['/nonexistent'])
